FROM node:14-alpine

USER root

# Update the package index and install older versions of packages
RUN apk update && \
    apk add --no-cache openssl curl 

WORKDIR /usr/src/app

COPY ./app/package*.json ./

# Install dependencies without checking for known vulnerabilities
RUN npm install --no-audit

COPY ./app .

# Install a global package with a known vulnerability
RUN npm install -g lodash@4.17.15

EXPOSE 3000

CMD ["npm", "start"]